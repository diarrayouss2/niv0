#!/usr/bin/bash
UI=mandenkan-ui
export AUTH=fkzfssLX_dFX2J5H8cAz
echo $AUTH
sed s/\$AUTH/"$AUTH"/g local.yml > tmp.yml

cp -v ../${UI}/build/ui-bundle.zip .
podman run -it --rm \
  --security-opt label=disable \
  -v `pwd`:/antora:z antora/antora  \
	--stacktrace \
	site.yml
	git commit -a -m "Built on $(date)" && git push origin master
	#--cache-dir=./.cache  \
