#!/usr/bin/bash

podman run -u $(id -u) \
  -v $PWD:/site:Z --rm -it \
  --userns=keep-id \
  -p 8080:8080 \
  asciidoctor-hugo bash
